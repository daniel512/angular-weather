import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Weather} from "./models/weather.model";

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private apiUrl = 'http://api.weatherapi.com/v1/current.json';
  private apiKey = 'c50d522f5ccf4073b4c180312210912';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Weather[]> {
    return this.http.get<Weather[]>('/assets/api/weather.json');
  }

  getByCity(city: string): Observable<Weather> {
    return this.http.get<Weather>(`${this.apiUrl}?key=${this.apiKey}&q=${city}`);
  }
}
