import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WeatherComponent} from './weather.component';
import {WeatherCardComponent} from './weather-card/weather-card.component';
import {WeatherCityComponent} from './weather-city/weather-city.component';
import {ReactiveFormsModule} from "@angular/forms";
import {WeatherDetailsComponent} from './weather-details/weather-details.component';
import {DragDropModule} from "@angular/cdk/drag-drop";


@NgModule({
  declarations: [
    WeatherComponent,
    WeatherCardComponent,
    WeatherCityComponent,
    WeatherDetailsComponent
  ],
  exports: [
    WeatherComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        DragDropModule
    ]
})
export class WeatherModule { }
