import {Component, OnInit} from '@angular/core';
import {Weather} from "./models/weather.model";
import {WeatherService} from "./weather.service";
import {LocationService} from "../core/services/location.service";
import {map, merge, mergeMap, toArray} from "rxjs";
import {CityService} from "./weather-city/city.service";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.sass']
})
export class WeatherComponent implements OnInit {

  weathers: Weather[] = [];

  constructor(
    private weatherService: WeatherService,
    private locationService: LocationService,
    private cityService: CityService,
  ) {
  }

  ngOnInit(): void {
    this.updateWeather();
  }

  onAddCity(city: string): void {
    this.cityService.add({name: city, index: 1});
    this.updateWeather();
  }

  onRemoveCity(city: string): void {
    this.cityService.remove({name: city, index: 1})
    this.updateWeather()
  }

  clearAll(): void {
    this.cityService.removeAll();
    this.updateWeather();
  }

  drop(event: CdkDragDrop<Weather[]>) {
    moveItemInArray(this.weathers, event.previousIndex, event.currentIndex);
    const city = this.weathers[event.currentIndex]
    this.cityService.update({name: city.location.name, index: event.currentIndex})
  }

  private updateWeather(): void {
    merge(
      this.cityService.get().sort(it => it.index).map(it => it.name),
      this.locationService.get().pipe(
        map(it => it.city)
      ),
    )
      .pipe(
        mergeMap(city => this.weatherService.getByCity(city)),
        toArray()
      )
      .subscribe(
        weathers => this.weathers = weathers,
        error => this.removeInvalidCity(WeatherComponent.getCityNameFromUrl(error.url))
      )
  }

  private removeInvalidCity(cityName: string): void {
    this.cityService.remove({name: cityName, index: 0})
  }

  private static getCityNameFromUrl(url: string): string {
    return url.split('q=')[1];
  }
}
