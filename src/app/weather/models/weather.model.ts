import {WeatherLocation} from "./weather-location.model";
import {WeatherCurrent} from "./weather-current.model";

export interface Weather {
  location: WeatherLocation;
  current: WeatherCurrent;
}
