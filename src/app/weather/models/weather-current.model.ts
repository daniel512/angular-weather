import {WeatherCondition} from "./weather-condition.model";

export interface WeatherCurrent {
  temp_c: number;
  condition: WeatherCondition;
  is_day: boolean;
}
