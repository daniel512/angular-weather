export interface WeatherLocation {
  name: string;
  region: string;
  country: string;
  localtime: string;
}
