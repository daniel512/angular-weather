export interface WeatherCondition {
  text: string;
  icon: string;
}
