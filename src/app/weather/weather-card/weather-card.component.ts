import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Weather} from "../models/weather.model";

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.sass']
})
export class WeatherCardComponent implements OnInit {

  constructor() { }

  @Input() data: Weather | undefined;
  hour: string | undefined;
  @Output() removeCity = new EventEmitter<string>();

  static hourFromLocalTime(time: string): string {
    return time.split(' ')[1];
  }

  ngOnInit(): void {
    this.hour = WeatherCardComponent.hourFromLocalTime(this.data!.location.localtime);
  }

  remove() {
    this.removeCity.emit(this.data?.location.name);
  }
}
