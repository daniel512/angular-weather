import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CityService} from "./city.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-weather-city',
  templateUrl: './weather-city.component.html',
  styleUrls: ['./weather-city.component.sass']
})
export class WeatherCityComponent implements OnInit {

  cityControl = new FormControl('');
  @Output() addCity = new EventEmitter<string>();

  constructor(
    private cityService: CityService
  ) { }

  ngOnInit(): void {
  }

  add(): void {
    this.addCity.emit(this.cityControl.value)
  }

}
