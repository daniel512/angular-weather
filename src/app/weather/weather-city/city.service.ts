import {Injectable} from '@angular/core';
import {City} from "../models/city.model";

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor() {
  }

  get(): City[] {
    if (localStorage.getItem('cities'))
      return JSON.parse(localStorage.getItem('cities')!);

    return [];
  }

  add(city: City): void {
    const cities = this.get();

    if (this.cityExists(city))
      return;

    cities.push({name: city.name.toLowerCase(), index: cities.length});
    localStorage.setItem('cities', JSON.stringify(cities));
  }

  update(city: City) {
    if (!this.cityExists(city))
      return;

    let cities = this.get();

    cities = cities.map(it => it.name.toLowerCase() === city.name.toLowerCase() ? city : it)

    localStorage.setItem('cities', JSON.stringify(cities))
  }

  remove(city: City): void {
    let cities = this.get();

    if (this.cityExists(city))
      cities = cities.filter(it => !it.name.includes(city.name.toLowerCase()))

    localStorage.setItem('cities', JSON.stringify(cities))
  }

  removeAll(): void {
    localStorage.removeItem('cities')
  }

  private cityExists(city: City): boolean {
    return this.get().find(it => it.name.toLowerCase().includes(city.name.toLowerCase())) !== void 0;
  }
}
