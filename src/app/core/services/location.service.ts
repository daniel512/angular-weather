import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private apiUrl = 'https://ipgeolocation.abstractapi.com/v1/';
  private apiKey = 'abf68c6e2ba648b1a5955af4fc11f0d4';

  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(`${this.apiUrl}?api_key=${this.apiKey}`);
  }
}
