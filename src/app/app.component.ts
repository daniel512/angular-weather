import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  data: any;

  ngOnInit(): void {
    this.data = fetch("https://reqres.in/api/users?page=2")
      .then(response => response.json())
  }


}
